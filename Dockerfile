FROM php:7.1-fpm-alpine3.9

LABEL "Maintainer"="Faiz Hasan" \
        "Email"="mfaizhasan92@gmail.com" \
        "Description"="Php images"

RUN apk add --no-cache --update $PHPIZE_DEPS \
    zlib-dev \
    libxml2-dev \
    libmcrypt-dev \
    shadow \
    supervisor \
    ffmpeg

RUN docker-php-ext-install \
        mysqli \
        pdo_mysql \
        mcrypt \
        zip \
        bcmath \
        pcntl

RUN apk del \
        make \
        g++ \
        musl-dev \
        gcc \
        mpc1 \
        mpfr3 \
        libatomic \
        libgomp \
        isl \
        gmp \
        binutils \
        autoconf \
        m4

RUN rm -rf /tmp/* /var/cache/apk/*
